<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="stylesheet" href="css/bootstrap.min.css">
  <link href="css/simple-sidebar.css" rel="stylesheet">
     <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
  <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>

  <title>Panel de administracion</title>

</head>

<body>

  <div class="d-flex" id="wrapper">
  
		<div class="bg-light border-right" id="sidebar-wrapper">
			  <div class="sidebar-heading">Granja El Tepetate </div>
			  <div class="list-group list-group-flush">
				<a href="./registro-gasto-ingreso.html" class="nav-link list-group-item-action bg-light">Registrar Gasto/Ingreso</a>
				<a href="./panel_administrador.php" 	class="nav-link bg-light">Capital de Trabajo</a>
				<a href="./manejo-usuarios.php" 		class="nav-link list-group-item-action bg-light">Manejo de Usuarios</a>
				<a href="./estado-granja.php"		 	class="nav-link list-group-item-action bg-light">Estado de la Granja</a>
				<a href="./promociones.php"		 		class="nav-link list-group-item-action bg-light">Promociones</a>
				<a href="./recetario.php"		 		class="nav-link list-group-item-action bg-light">Recetario</a>
				<a href="./comentarios.php" 			class="nav-link list-group-item-action bg-light">Comentarios de Visitantes</a>
				<a href="./alimento.php"		 		class="nav-link list-group-item-action bg-light">Monitoreo alimento</a> 
			  </div>
		</div>
	
		<div id="page-content-wrapper">
		  <nav class="navbar navbar-expand-lg navbar-light bg-dark border-bottom">
			<button class="btn btn-outline-secondary dropdown-toggle" id="menu-toggle">Menu</button>
			<ul class="navbar-nav ml-auto mt-2 mt-lg-0">
				<li class="nav-item text-nowrap">
					<a class="nav-link" style="color:white;" href="./index.php">Cerrar Sesión</a>
				</li>
			</ul>
		  </nav>
		  
		  <!----CONTENIDO---->
		   <main role="main">
      <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2">Contenido Seleccionado</h1>
        <div class="col-auto my-1">
          <label class="mr-sm-2 sr-only" for="inlineFormCustomSelect">Preference</label>
          <select class="custom-select mr-sm-2" id="inlineFormCustomSelect">
            <option selected>Esta semana</option>
            <option value="1">Este mes</option>
            <option value="2">Este año</option>
            <option value="3">El año pasado</option>
          </select>
        </div>  
        <div class="col-auto my-1">
          <label class="mr-sm-2 sr-only" for="inlineFormCustomSelect">Preference</label>
          <select class="custom-select mr-sm-2" id="inlineFormCustomSelect">
            <option selected>Todas las transacciones</option>
            <option value="1">Solo gastos</option>
            <option value="2">Solo ingresos</option>
          </select>
        </div>
      </div>
      

       <?php
      require('./servicios/conection.php');

          $query = "SELECT * FROM transacciones";
          $result = mysqli_query($link,$query);


                echo "<div class=\"table-responsive\">";
                echo "<table class=\"table table-striped table-sm\">";
                    echo "<thead>";
                      echo"<tr>";
                        echo"<th>Fecha</th>";
                        echo"<th>Transacci&oacute;n</th>";
                        echo"<th>Cantidad</th>";
                        echo"<th>Concepto</th>";
                      echo"</tr>";
                    echo"</thead>";
                    echo"<tbody>";
                    foreach ($result as $key => $value) {
                              echo "<tr>";
                              echo "<td>".$value['fecha']."</td>";
                              echo "<td>".$value['tipo']."</td>";
                              echo "<td>".$value['cantidad']."</td>";
                              echo "<td>".$value['concepto']."</td>";
                              echo "</tr>";
                            }
                    echo "</tbody>";
                  echo"</table>";
                  echo"<p><span>Gastos Totales:</span></p>";
                  echo"<p><span>Ingresos Totales:</span></p>";
                  echo"<p><span>Ingresos - Gastos:</span></p>";
                echo"</div>";

                  
                  $link->close();
                  
              ?>
      <br><br>
      <div class="form-group">
        <label for="exampleFormControlTextarea1">Ingrese la cantidad que espera ganar por trucha</label>
        <input class="form-control" id="exampleFormControlTextarea1" rows="3"></input>
      </div>
      <button type="button" class="btn btn-primary">Calcular</button>
	  </main>
		</div>		
  </div>
  
  <script src="js/jquery.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script>
    $("#menu-toggle").click(function(e) {
      $("#wrapper").toggleClass("toggled");
    });
  </script>

</body>

</html>
