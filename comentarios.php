<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="stylesheet" href="css/bootstrap.min.css">
  <link href="css/simple-sidebar.css" rel="stylesheet">
	<title>Comentarios de Visitantes</title>

</head>

<body>

  <div class="d-flex" id="wrapper">
  
		<div class="bg-light border-right" id="sidebar-wrapper">
			  <div class="sidebar-heading">GRANJA EL TEPETATE</div>
			    <div class="list-group list-group-flush">
				<a href="./registro-gasto-ingreso.html" class="nav-link list-group-item-action bg-light">Registrar Gasto/Ingreso</a>
				<a href="./panel_administrador.php" 	class="nav-link list-group-item-action bg-light">Capital de Trabajo</a>
				<a href="./manejo-usuarios.php" 		class="nav-link list-group-item-action bg-light">Manejo de Usuarios</a>
				<a href="./estado-granja.php"		 	class="nav-link list-group-item-action bg-light">Estado de la Granja</a>
				<a href="./promociones.php"		 		class="nav-link list-group-item-action bg-light">Promociones</a>
				<a href="./recetario.php"		 		class="nav-link list-group-item-action bg-light">Recetario</a>
				<a href="./comentarios.php" 			class="nav-link bg-light">Comentarios de Visitantes</a>
				<a href="./alimento.php"		 		class="nav-link list-group-item-action bg-light">Monitoreo alimento</a> 
			  </div>
		</div>
	
		<div id="page-content-wrapper">
		  <nav class="navbar navbar-expand-lg navbar-light bg-dark border-bottom">
			<button class="btn btn-outline-secondary dropdown-toggle" id="menu-toggle">Menu</button>
			<ul class="navbar-nav ml-auto mt-2 mt-lg-0">
				<li class="nav-item text-nowrap">
					<a class="nav-link" style="color:white;" href="./index.php">Cerrar Sesión</a>
				</li>
			</ul>
		  </nav>
		  
		  <!----CONTENIDO---->
		  <main role="main">
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto|Varela+Round">
  <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
 
 
  </head>
  <body>
      <div class="container">
          <div class="table-wrapper">
              <div class="table-title">
                  <div class="row">
                      <div class="col-sm-6">
                        <h2>Comentarios de visitantes</h2>
                      </div>
                      <div class="col-sm-6">
                       <a href="#deleteEmployeeModal" class="btn btn-danger" data-toggle="modal"><i class="material-icons">&#xE15C;</i> <span>Eliminar Comentario</span></a>
                       </div>
                  </div>
              </div>
              <?php  
  require('./servicios/conection.php');
 $output = '';  
 $sql = "SELECT * FROM comentarios ORDER BY id_comentario ASC";  
 $result = mysqli_query($link, $sql);   
     echo '<div class="table-responsive">  
           <table class="table table-striped table-hover">  
            <thead>
                <tr>
                          <th>ID </th>
                          <th>Correo </th>
                          <th>Fecha </th>
                          <th>Comentario</th>
                      </tr>
                </thead>';
                echo "<tbody>";
                 while($row = mysqli_fetch_assoc($result)){
                echo "<tr>";
                    echo "<td>".$row["id_comentario"]."</td>";
                    echo "<td>".$row["correo"]."</td>";
                    echo "<td>".$row["fecha"]."</td>";
                    echo "<td>".$row["comentario"]."</td>";
                echo "</tr>";
            }
        echo "</tbody>";
    echo "</table>";
    echo "</div>" ;
    ?>

    <!-- Delete Modal HTML -->
    <div id="deleteEmployeeModal" class="modal fade">
      <div class="modal-dialog">
        <div class="modal-content">
          <form action="./servicios/delete_comentario.php" method="POST">
            <div class="modal-header">            
              <h4 class="modal-title">Eliminar Comentario</h4>
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
           <div class="modal-body">
                <label>ID del comentario</label>
                <input type="text" class="form-control" list="idsp" name="idc">
                  <datalist id="idsp">
                      <?php
                       require('./servicios/conection.php');
                      $query="SELECT id_comentario FROM comentarios";
                      $result=mysqli_query($link,$query);
                      while($row = mysqli_fetch_assoc($result)){
                          echo "<option value=".$row["id_comentario"].">";
                              }
                    ?>
                  </datalist>
              </div>
            <div class="modal-footer">
              <input type="button" class="btn btn-default" data-dismiss="modal" value="Cancelar">
              <input type="submit" class="btn btn-danger" value="Eliminar">
            </div>
          </form>
        </div>
      </div>
    </div>
  </body>
  </html>                                		                            
</main>
		  
		</div>		
  </div>
  
  <script src="js/jquery.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script>
    $("#menu-toggle").click(function(e) {
      $("#wrapper").toggleClass("toggled");
    });
  </script>

</body>

</html>
