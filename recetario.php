<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="stylesheet" href="css/bootstrap.min.css">
  <link href="css/simple-sidebar.css" rel="stylesheet">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
	<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>

  <title>Recetario</title>

</head>

<body>

  <div class="d-flex" id="wrapper">
  
		<div class="bg-light border-right" id="sidebar-wrapper">
			  <div class="sidebar-heading">Granja El Tepetate </div>
			  <div class="list-group list-group-flush">
				<a href="./registro-gasto-ingreso.html" class="nav-link list-group-item-action bg-light">Registrar Gasto/Ingreso</a>
				<a href="./panel_administrador.php" 	class="nav-link list-group-item-action bg-light">Capital de Trabajo</a>
				<a href="./manejo-usuarios.php" 		class="nav-link list-group-item-action bg-light">Manejo de Usuarios</a>
				<a href="./estado-granja.php"		 	class="nav-link list-group-item-action bg-light">Estado de la Granja</a>
				<a href="./promociones.php"		 		class="nav-link list-group-item-action bg-light">Promociones</a>
				<a href="./recetario.php"		 		class="nav-link bg-light">Recetario</a>
				<a href="./comentarios.php" 			class="nav-link list-group-item-action bg-light">Comentarios de Visitantes</a>
				<a href="./alimento.php"		 		class="nav-link list-group-item-action bg-light">Monitoreo alimento</a> 
			  </div>
		</div>
	
		<div id="page-content-wrapper">
		  <nav class="navbar navbar-expand-lg navbar-light bg-dark border-bottom">
			<button class="btn btn-outline-secondary dropdown-toggle" id="menu-toggle">Menu</button>
			<ul class="navbar-nav ml-auto mt-2 mt-lg-0">
				<li class="nav-item text-nowrap">
					<a class="nav-link" style="color:white;" href="./index.php">Cerrar Sesión</a>
				</li>
			</ul>
		  </nav>
		  
		  <!----CONTENIDO---->
		  <main role="main">
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto|Varela+Round">
  <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
 
 
 
 <script type="text/javascript">
  $(document).ready(function(){
    // Activate tooltip
    $('[data-toggle="tooltip"]').tooltip();
    
    // Select/Deselect checkboxes
    var checkbox = $('table tbody input[type="checkbox"]');
    $("#selectAll").click(function(){
      if(this.checked){
        checkbox.each(function(){
          this.checked = true;                        
        });
      } else{
        checkbox.each(function(){
          this.checked = false;                        
        });
      } 
    });
    checkbox.click(function(){
      if(!this.checked){
        $("#selectAll").prop("checked", false);
      }
    });
  });
  </script>



  </head>
  <body>
      <div class="container">
          <div class="table-wrapper">
              <div class="table-title">
                  <div class="row">
                      <div class="col-sm-6">
                        <h2>Recetario</h2>
                      </div>
            <div class="col-sm-6">
              <a href="#addEmployeeModal" class="btn btn-success" data-toggle="modal"><i class="material-icons">&#xE147;</i> <span>Agregar Receta</span></a>
              <a href="#deleteEmployeeModal" class="btn btn-danger" data-toggle="modal"><i class="material-icons">&#xE15C;</i> <span>Eliminar Receta(s)</span></a>						
            </div>
                  </div>
              </div>
              <?php
         require('./servicios/conection.php');

          $query = "SELECT * FROM recetas";
          $result = mysqli_query($link,$query);


                echo "<div class=\"table-responsive\">";
                echo "<table class=\"table table-striped table-sm\">";
                    echo "<thead>";
                      echo"<tr>";
                        echo"<th>ID</th>";                        
                        echo"<th>Nombre</th>";
						echo"<th>Tiempo</th>";                        
						echo"<th>Dificutad</th>";
						echo"<th>Ingredientes</th>";
						echo"<th>Preparacion</th>";
						echo"<th>Opciones</th>";
                      echo"</tr>";
                    echo"</thead>";
                    echo"<tbody>";
                    foreach ($result as $key => $value) {
                              echo "<tr>";
                              echo "<td>".$value['id_receta']."</td>";                              
                              echo "<td>".$value['nombre']."</td>";
							  echo "<td>".$value['tiempo']."</td>";                              
							  echo "<td>".$value['dificultad']."</td>";
							  echo "<td>".$value['ingredientes']."</td>";
							  echo "<td>".$value['preparacion']."</td>";
							  echo"<td>";
                              echo"<a href=\"#editEmployeeModal\" class=\"edit\" data-toggle=\"modal\"><i class=\"material-icons\" data-toggle=\"tooltip\" title=\"Edit\">&#xE254;</i></a>";
                              echo"<a href=\"#deleteEmployeeModal\" class=\"delete\" data-toggle=\"modal\"><i class=\"material-icons\" data-toggle=\"tooltip\" title=\"Delete\">&#xE872;</i></a>";
							  echo"</td>";
                              echo "</tr>";
                            }
                    echo "</tbody>";
                  echo"</table>";
                echo"</div>";
          
                  $link->close();  
              ?>
                      </tr>
                  </tbody>
              </table>
      </div>
    <!-- Add Modal HTML -->
    <div id="addEmployeeModal" class="modal fade">
      <div class="modal-dialog">
        <div class="modal-content">
          <form method="POST" action="./servicios/agregar_receta.php">					  
            <div class="modal-header">						
              <h4 class="modal-title">Agregar Receta</h4>
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
            <div class="modal-body">					
              <div class="form-group">
                <label>Nombre</label>
                <input type="text" class="form-control" name="nombre" id="nombre" required>
              </div>
              <div class="form-group">
                <label>Tiempo de Preparaci&oacute;n</label>
                <input type="text" class="form-control" name="tiempo" id="tiempo"required>
              </div>
              <div class="form-group">
                <label>Dificutad</label>
                <input type="text" class="form-control" name="dificultad" id="dificultad"required>
              </div>
              <div class="form-group">
                <label>Ingredientes</label>
                <textarea class="form-control" name="ingredientes" id="ingredientes"required></textarea>
              </div>
              <div class="form-group">
                <label>Preparaci&oacute;n</label>
                <textarea class="form-control" name="preparacion" id="preparacion"required></textarea>
              </div>					
            </div>
            <div class="modal-footer">
              <input type="button" class="btn btn-default" data-dismiss="modal" value="Cancelar">
              <input type="submit" class="btn btn-success" value="Agregar" name="Agregar" id="Agregar">
            </div>
          </form>
        </div>
      </div>
    </div>
    <!-- Edit Modal HTML -->
    <div id="editEmployeeModal" class="modal fade">
      <div class="modal-dialog">
        <div class="modal-content">
          <form method="POST" action="./servicios/editar_receta.php">
            <div class="modal-header">						
              <h4 class="modal-title">Editar Receta</h4>
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
            <div class="modal-body">					
              <div class="form-group">
                <label>Nombre</label>
                <input type="text" class="form-control" name="nombre" id="nombre"required>
              </div>
              <div class="form-group">
                <label>Tiempo de preparaci&oacute;n</label>
                <input type="text" class="form-control" name="tiempo" id="tiempo"required>
              </div>
              <div class="form-group">
                <label>Ingredientes</label>
                <textarea class="form-control" name="ingredientes" id="ingredientes"required></textarea>
              </div>
              <div class="form-group">
                <label>Preparaci&oacute;n</label>
                <input type="text" class="form-control" name="preparacion" id="preparacion"required>
              </div>					
            </div>
            <div class="modal-footer">
              <input type="button" class="btn btn-default" data-dismiss="modal" value="Cancelar">
              <input type="submit" class="btn btn-info" value="Guardar" name="Guardar" id="Guardar">
            </div>
          </form>
        </div>
      </div>
    </div>
    <!-- Delete Modal HTML -->
    <div id="deleteEmployeeModal" class="modal fade">
      <div class="modal-dialog">
        <div class="modal-content">
          <form method="POST" action="./servicios/eliminar_receta.php">
            <div class="modal-header">						
              <h4 class="modal-title">Eliminar Receta</h4>
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
            <div class="modal-body">
			  <div class="form-group">
                <label>ID</label>
                <input type="text" class="form-control" name="id" id="id"required>
              </div>
              <p>¿Está seguro que desea eliminar el elemento seleccionado?</p>
              <p class="text-warning"><small>Esta accion no puede deshacerse a menos que consulte al desarrollador.</small></p>
            </div>
            <div class="modal-footer">
              <input type="button" class="btn btn-default" data-dismiss="modal" value="Cancelar">
              <input type="submit" class="btn btn-danger" value="Eliminar" name="Eliminar" id="Eliminar">
            </div>
          </form>
        </div>
      </div>
    </div>
  </body>
  </html>                                		                            
</main>
		</div>		
  </div>
  
  <script src="js/jquery.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script>
    $("#menu-toggle").click(function(e) {
      $("#wrapper").toggleClass("toggled");
    });
  </script>

</body>

</html>
