<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="stylesheet" href="css/bootstrap.min.css">
  <link href="css/simple-sidebar.css" rel="stylesheet">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
	<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
	<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
	<script src="./charts/Chart.min.js"></script>
	<script src="./charts/utils.js"></script>

  <title>Estado de la granja</title>
</head>

<body>

  <div class="d-flex" id="wrapper">
  
		<div class="bg-light border-right" id="sidebar-wrapper">
			  <div class="sidebar-heading">Granja El Tepetate </div>
			   <div class="list-group list-group-flush">
				<a href="./registro-gasto-ingreso.html" class="nav-link list-group-item-action bg-light">Registrar Gasto/Ingreso</a>
				<a href="./panel_administrador.php" 	class="nav-link list-group-item-action bg-light">Capital de Trabajo</a>
				<a href="./manejo-usuarios.php" 		class="nav-link list-group-item-action bg-light">Manejo de Usuarios</a>
				<a href="./estado-granja.php"		 	class="nav-link bg-light">Estado de la Granja</a>
				<a href="./promociones.php"		 		class="nav-link list-group-item-action bg-light">Promociones</a>
				<a href="./recetario.php"		 		class="nav-link list-group-item-action bg-light">Recetario</a>
				<a href="./comentarios.php" 			class="nav-link list-group-item-action bg-light">Comentarios de Visitantes</a>
				<a href="./alimento.php"		 		class="nav-link list-group-item-action bg-light">Monitoreo alimento</a> 
			  </div>
		</div>
	
		<div id="page-content-wrapper">
		  <nav class="navbar navbar-expand-lg navbar-light bg-dark border-bottom">
			<button class="btn btn-outline-secondary dropdown-toggle" id="menu-toggle">Menu</button>
			<ul class="navbar-nav ml-auto mt-2 mt-lg-0">
				<li class="nav-item text-nowrap">
					<a class="nav-link" style="color:white;" href="./index.php">Cerrar Sesión</a>
				</li>
			</ul>
		  </nav>
		  
		  <!----CONTENIDO---->
		 <main role="main">
<!--Se llena la tabla-->
<?php
         require('./servicios/conection.php');

          $query = "SELECT id_estanque, num_truchas, talla, peso, biomasa FROM estanque";
          $result = mysqli_query($link,$query);


                echo "<div class=\"table-responsive\">";
                echo "<table class=\"table table-striped table-sm\">";
                    echo "<thead>";
                      echo"<tr>";
                       
                        echo"<th>ID Estanque</th>";
                        echo"<th>Cantidad de Truchas</th>";
                        echo"<th>Talla/cm</th>";
                        echo"<th>Peso/gr</th>";
                        echo"<th>Biomasa/kg</th>";
                      echo"</tr>";
                    echo"</thead>";
                    echo"<tbody>";
                    foreach ($result as $key => $value) {
                              echo "<tr>";
                              echo "<td>".$value['id_estanque']."</td>";
                              echo "<td>".$value['num_truchas']."</td>";
                              echo "<td>".$value['talla']."</td>";
                              echo "<td>".$value['peso']."</td>";
                               echo "<td>".$value['biomasa']."</td>";
                              echo "</tr>";
                            }
                    echo "</tbody>";
                  echo"</table>";
                echo"</div>";

                  
                  $link->close();
                  
              ?>

			<div class="col-sm-6">
              <a href="#addEmployeeModal" class="btn btn-success" data-toggle="modal"><i class="material-icons">&#xE147;</i> <span>Actualizar información del estanque</span></a>          
            </div>


<canvas id="myChart"></canvas>
<br>
<?php
	      require('./servicios/conection.php');

          $query = "SELECT id_estanque,biomasa FROM estanque";
          $result = mysqli_query($link,$query);
          $valoresY=array();
          $valoresX=array();
          while ($ver=mysqli_fetch_row($result)) {
          		$valoresY[]=$ver[1];
          		$valoresX[]=$ver[0];
          }
          $datosX=json_encode($valoresX);
          $datosY=json_encode($valoresY);

?>
<script type="text/javascript">
	function crearCadena(json){
		var parsed = JSON.parse(json);
		var arr=[];
		for(var x in parsed){
			arr.push(parsed[x]);
		}
		return arr;
	}
</script>
<script> 
    
datosX=crearCadena('<?php echo $datosX ?>');
datosY=crearCadena('<?php echo $datosY ?>');

var ctx1 = document.getElementById('myChart').getContext('2d');
var chart = new Chart(ctx1, {
    type: 'line',
    data: {
        labels: datosX,
        datasets: [
        {
            label: 'Biomasa del Estanque: ',
            backgroundColor: '#42a5f5',
            borderColor: 'gray',
            data: datosY,
            fill:false,
        }
		]},
    options: {
      responsive: true
    },
    tooltips: {
					mode: 'index',
					intersect: false,
				},
				hover: {
					mode: 'nearest',
					intersect: true
				},
				scales: {
					xAxes: [{
						display: true,
						scaleLabel: {
							display: true,
							labelString: 'Month'
						}
					}],
					yAxes: [{
						display: true,
						scaleLabel: {
							display: true,
							labelString: 'Value'
						}
					}]
				}
});


</script>
  

  <div id="addEmployeeModal" class="modal fade">
      <div class="modal-dialog">
        <div class="modal-content">
          <form method="POST" action="./servicios/update_estanque.php">
            <div class="modal-header">            
              <h4 class="modal-title">Editar información</h4>
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
            <div class="modal-body">          
              <div class="form-group">
                <label>Estanque</label>
                <input type="text" class="form-control" list="idsp" name="idsp" id="isdp">
                  <datalist id="idsp">
                      <?php
                     require('./servicios/conection.php');
                      $query="SELECT id_estanque FROM estanque";
                      $result=mysqli_query($link,$query);
                      while($row = mysqli_fetch_assoc($result)){
                          echo "<option value=".$row["id_estanque"].">";
                              }
                    ?>
                  </datalist>
              </div>
              <div class="form-group">
                <label>Cantidad de Truchas</label>
                <input type="text" class="form-control" id="cantidadTruchas" name="cantidadTruchas1" required>
              </div>
              <div class="form-group">
                <label>Talla</label>
                <input type="text" class="form-control" id="talla" name="talla1" required>
              </div>
              <div class="form-group">
                <label>Peso</label>
                <input type="text" class="form-control" id="peso" name="peso1" required>
              </div>         
            </div>
            <div class="modal-footer">
              <input type="button" class="btn btn-default" data-dismiss="modal" value="Cancelar">
              <input type="submit" class="btn btn-success" value="Aceptar">
                  
          </form>
        </div>
      </div>
    </div>
</main> 
		</div>		
  </div>
  
  <script src="js/jquery.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script>
    $("#menu-toggle").click(function(e) {
      $("#wrapper").toggleClass("toggled");
    });
  </script>

</body>

</html>
