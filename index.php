<!DOCTYPE html>
<html lang="es">
<link href="https://fonts.googleapis.com/css2?family=Roboto&display=swap" rel="stylesheet">
<link href="fonts/style.css" rel="stylesheet">

<head>
	<meta charset="UTF-8">
	 <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<title>El Tepetate</title>
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<link rel="stylesheet" href="css/estilos.css">
</head>


<body>

		<header class="header">
			<!-- Image and text -->
<nav  class="navbar navbar-expand-lg navbar-dark">

  <a class="navbar-brand" href="#">
    <img src="img/logo.png" width="80" height="80" class="d-inline-block align-center" alt="">
  </a>
  <div class="ml-0 mr-4">
            <h4 class="text-light">GRANJA</h4>
            <h4 class="font-weight-bold text-light">EL TEPETATE</h4>
          </div>

<button class="navbar-toggler navbar-right" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

<div class="collapse navbar-collapse" id="navbarNav">
    <ul class="navbar-nav">
      <li class="nav-item" align="center" style=" width:100px; border-left: 1px solid rgba(255, 255, 255, 0.4);">
        <a class="nav-link" href="#top">Inicio</a>
      </li>
      <li class="nav-item" align="center"style="width:150px; border-left: 1px solid rgba(255, 255, 255, 0.4);">
        <a class="nav-link" href="#quienes">Quienes Somos</a>
      </li>
      <li class="nav-item"align="center" style="width:100px; border-left: 1px solid rgba(255, 255, 255, 0.4);">
        <a class="nav-link" href="#productos">Productos</a>
      </li>
      <li class="nav-item" align="center"style="width:100px; border-left: 1px solid rgba(255, 255, 255, 0.4);  border-right: 1px solid rgba(255, 255, 255, 0.4);">
        <a class="nav-link" href="#contac" >Contacto</a>
      </li>
    </ul>
    
  </div>
  <form action="" class="navbar-form navbar-right">
    	<div class="form-group">
        <a href="./login2.html"><button type="button" class="btn btn-light">Iniciar Sesi&oacute;n</button>    </a>
				</div>
    </form>
</nav>
		</header>
<!--------------------------------------Barra lateral de redes sociales-------------------------------------->
<div class="social-bar">
		<ul>
		<li><a href="https://www.facebook.com/Granja-de-Trucha-Tepetates-1125220357665723/" target="_blank" class="icon-facebook2"></a></li>
		<li><a href="#contac" class="icon-whatsapp"></a></li>
	    <li><a href="mailto:arturoalcocermolina@gmail.com" target="_blank" class="icon-envelope"></a></li>
	    <li><a href="https://www.google.com.mx/maps/@19.77103,-101.1240307,3a,60y,264.59h,82.64t/data=!3m6!1e1!3m4!1swKPHgeJUzPRwzON0bDjGNA!2e0!7i13312!8i6656" target="_blank" class="icon-location"></a></li>

		</ul>
    </div>

		<section class="main container">
			<div id="row1" class="row  justify-content-center">
				<div class="bd-example">
  <div id="carouselExampleCaptions" class="carousel slide" data-ride="carousel">
    <ol class="carousel-indicators">
      <li data-target="#carouselExampleCaptions" data-slide-to="0" class="active"></li>
      <li data-target="#carouselExampleCaptions" data-slide-to="1"></li>
      <li data-target="#carouselExampleCaptions" data-slide-to="2"></li>
      <li data-target="#carouselExampleCaptions" data-slide-to="3"></li>

    </ol>
    <div class="carousel-inner">
      <div class="carousel-item active">
        <img src="img/T7.jpg" class="d-block w-100" alt="...">
        <div class="carousel-caption d-none d-md-block">
          <h5></h5>
          <p></p>
        </div>
      </div>
      <div class="carousel-item">
        <img src="img/T8.jpg" class="d-block w-100" alt="...">
        <div class="carousel-caption d-none d-md-block">
          <h5></h5>
          <p></p>
        </div>
      </div>
      <div class="carousel-item">
        <img src="img/T9.jpg" class="d-block w-100" alt="...">
        <div class="carousel-caption d-none d-md-block">
          <h5></h5>
          <p></p>
        </div>
      </div>
      <div class="carousel-item">
        <img src="img/T4.jpg" class="d-block w-100" alt="...">
        <div class="carousel-caption d-none d-md-block">
          <h5></h5>
          <p></p>
        </div>
      </div>
    </div>
    <a class="carousel-control-prev" href="#carouselExampleCaptions" role="button" data-slide="prev">
      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carouselExampleCaptions" role="button" data-slide="next">
      <span class="carousel-control-next-icon" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>
</div>
</div>
 <! -- ************************************************************************************ -->

 <hr class="featurette-divider">

 <! -- ************************************************************************************ -->
<div id="contenedor" class="container bg-white card border-5 shadow my-3">
    			 
    			  <div class="container">
    				<section id="articulo" class="main row">
    				  <article  class="col-md-7">
    					<h1>Beneficios de la Trucha</h1>
    					<p align="justify">La trucha es un pescado rico en &aacute;cidos grasos omega 3, los cuales ayudan a prevenir enfermedades cardiovasculares al reducir la hipertensi&oacute;n y el colesterol. La trucha posee importantes propiedades nutricionales que son beneficiosas para una buena salud, adem&aacute;s de tener muy poca grasa. <br><br>¿Te animas a probarla?</p>
    				  </article>
    				  <aside class="col-md-5">
    					<img src="img/T5.jpg" class="d-block w-100 img-thumbnail" style="max-width:350px;" alt="...">
    				  </aside>
    				</section>
    			  </div>
    			

    			  <hr id="3P">
    			 
    			  <div class="container">
    				<section id="articulo" class="main row">
    				  <aside class="col-md-5">
    					<img style="max-width:350px;" src="img/T6.jpg" alt="" class="d-block w-100 img-thumbnail">
    				  </aside>
    				  <article class="col-md-7">
    					<h1>Empresa Orgullosamente Certificada</h1>
    					<p align="justify">Las Buenas Pr&aacute;cticas de Producci&oacute;n Acu&iacute;cola de Trucha consisten en aplicar una serie de recomendaciones, normas y actividades espec&iacute;ficas durante el proceso de producci&oacute;n. Su correcta aplicaci&oacute;n garantiza que la trucha cuente con las especificaciones de calidad sanitaria e inocuidad requeridas para el consumo humano. <br>Clave: AC-PD-16-19-473</p>
    				  </article>
    				</section>
    			  </div>
    			  
    			  <hr id="1P">
    			  
    			  <div class="container">
    				<section id="articulo" class="main row">
    				  <article class="col-md-7">
    					<h1 id="quienes">¿Sab&iacute;as que...?</h1>
    					<p align="justify">La trucha es una buena fuente de &aacute;cidos grasos omega 3, adem&aacute;s de ser un pescado rico en selenio, f&oacute;sforo, potasio o magnesio y vitaminas del grupo B. <br>Cien gramos de trucha aportan 90 calor&iacute;as y s&oacute;lo 3 gramos de grasa. <br><br>Si quieres bajar de peso incl&uacute;yelo en tu dieta.
							</p>
    				  </article>
    				  <aside class="col-md-5">
    					<img src="img/T7.jpg" class="d-block w-100 img-thumbnail" style="max-width:350px;" alt="...">
    				  </aside>
    				</section>
    			  </div>    
</div>
<!-- ************************************************************************************ -->
 <hr class="featurette-divider">
<div class="container">
  <h2>Acerca de Nosotros</h2>
  <p align = "justify" style="font-size:130%;">Somos una empresa de Agostitl&aacute;n, Michoac&aacute;n, dedicada a la producci&oacute;n de trucha arco&iacute;ris y  dispuestos a mejorar la alimentaci&oacute;n sana al proveer a nuestros clientes de un producto que posee varias vitaminas y minerales importantes que son importantes para prevenir enfermedades coronarias, minimizar accidentes vasculares cerebrales, mejora las respuestas inmunológicas y reduce el riesgo de enfermedades degenerativas.
  </p>
  <ul>
		</li>Estamos certificados por la Secretar&iacute;a de Agricultura y Desarrollo Rural Clave: AC-PD-16-19-473.</li>
	<ul>
	<ul>
		</li>M&aacute;s de 20 años de experiencia nos respaldan.</li>
	<ul>
  <br>
  <div class="row">
    <div class="bg-primary text-light col-md-6">
      <h3>Misi&oacute;n</h3>
      <p align = "justify"style="font-size:120%;">En Granja El Tepetate nos esmeramos d&iacute;a a d&iacute;a para aplicar las buenas pr&aacute;cticas de producci&oacute;n de trucha arcor&iacute;is, con ello ofrecemos a nuestros clientes un producto saludable y nutritivo de  alta calidad e inocuidad.</p>
	  </div>
    <div class="col-md-6">
      <h3 id="productos">Visi&oacute;n</h3>
      <p align = "justify"style="font-size:120%;">La Granja El Tepetate es reconocida por su producci&oacute;n de trucha arco&iacute;ris de alta calidad e inocuidad; sus m&eacute;todos de producci&oacute;n est&aacute;n apegados a est&aacute;ndares que permiten mantener una responsabilidad y equilibrio del medio ambiente natural, econ&oacute;mico y social. </p>
    </div>
  </div>
</div>
   <hr class="featurette-divider">
<!-- ************************************************************************************ -->
 <div class="container marketing" >
    <div class="row">
	
	
      <div class="col-lg-4" align="center">
		 <img src="img/promociones.png" width="140" height="140" background="#777" color="#777" class="rounded-circle">
        <h3 align="center">Promociones</h3>
        <p align="justify">Una serie de promociones proporcionada por la granja, esperamos sean de su agrado.</p>
       <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modalcomprar">Mostrar detalles</button>
      </div>
	  
	  <!--------------------------------PRESENTACIONES-------------------------->
      <div class="col-lg-4" align="center">
        <img src="img/promociones_truchas.jpg" width="140" height="140" background="#777" color="#777" class="rounded-circle">
        <h3 align="center">Presentaciones</h3>
        <p align="justify">Las distintas presentaciones en las cuales ofrecemos la trucha.</p>
		<button type="button" class="btn btn-primary" data-toggle="collapse" data-target="#collapse">Mostrar detalles</button>	 
      </div>
	  
	  
      <div class="col-lg-4" align="center">
        <img src="img/recetario_truchas.jpg" width="140" height="140" background="#777" color="#777" class="rounded-circle">
        <h3 align="center">Recetario</h3>
        <p align="justify">¿No sabe c&oacute;mo preparar el producto que compra? Dentro de este recetario encontrar&aacute; recetas variadas, para elaborar deliciosos platillos con el producto comprado.</p>
       <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modalrecetas">Abrir Recetario</button>
      </div>
    </div>
	
	
	  <div id="collapse" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
							     <hr class="featurette-divider">
						 <div id="contenedor" class="row">			 
    			  <div class="container">
    				<section id="articulo" class="main row">
    				  <article  class="col-md-7">
    					<h1>Presentaciones</h1>
    					<p>Contamos con diferentes presentaciones, te presentamos m&aacute;s detalles a continuaci&oacute;n. </p>
    				  </article>
    				</section>
    			  </div>
    			  <hr id="3P">
						  
				    <div class="row">
    				 <div class="card " style="width: 22%; margin:1.5%;">
					  <img class="card-img-top"height="190" src="img/Filete_salmonado.jpg" alt="Card image cap">
					  <div class="card-body">
						<h5 class="card-title">Filete de trucha salmonado</h5>
						<p class="card-text">Nuestra distinguida trucha arco&iacute;ris, salmonada.</p>
					  </div>
					</div>	
							
						 <div class="card " style="width: 22%; margin:1.5%;">
					  <img class="card-img-top" height="190"src="img/mariposa_deshuesado.jpg" alt="Card image cap">
					  <div class="card-body">
						<h5 class="card-title">Estilo corte mariposa deshuesado</h5>
						<p class="card-text">Deliciosa trucha con corte mariposa sin hueso.</p>
					  </div>
					</div>	
					
					 <div class="card " style="width: 22%; margin:1.5%;">
					  <img class="card-img-top" height="190"src="img/mariposa_blanca.jpg" alt="Card image cap">
					  <div class="card-body">
						<h5 class="card-title">Mariposa blanca</h5>
						<p class="card-text"><br>Trucha arco&iacute;ris estilo mariposa blanca.</p>
					  </div>
					</div>	
					
					 <div class="card " style="width: 22%; margin:1.5%;">
					  <img class="card-img-top" src="img/TA2.jpg" alt="Card image cap">
					  <div class="card-body">
						<h5 class="card-title">Trucha ahumada salmonada</h5>
						<p class="card-text">La misma trucha salmonada, ahora ahumada.</p>
					  </div>
					</div>	
					
					 <div class="card " style="width: 22%; margin:1.5%;">
					  <img class="card-img-top" height="190"  src="img/TAB.jpg" alt="Card image cap">
					  <div class="card-body">
						<h5 class="card-title">Trucha ahumada blanca</h5>
						<p class="card-text">Trucha arco&iacute;ris blanca ahumada.</p>
					  </div>
					</div>	
    			  </div>  
    			    
					</div>
						</div>
	
	</section>

<! -- ************************************STARTPOPUP************************************** -->


							<!-- Modal Comprar-->
<div class="modal fullscreen-modal fade" id="modalcomprar" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg"  role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Promociones</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
       <?php  
 require('./servicios/conection.php');
 $img=0;
 $sql = "SELECT * FROM promociones ORDER BY id_promociones ASC";  
 $result = mysqli_query($link, $sql);   
     
                 while($row = mysqli_fetch_assoc($result)){
                  echo '<div class="card mb-3" >
                <div class="row no-gutters" style="background-color: rgb(245,245,245);">
                <div class="col-md-8" >
                  <div class="card-body" >
                  <div class="card-group" >
                    <div class="card-body text-justify" style="margin: 1px; padding: 1px;">';
                echo " <h5 class='card-title'>".$row["nombre"]."</h5>";
                    echo "<p><a><i></i></a> $".$row["precio"]."</p>";
                    echo "<p class='card-text'><small class='text-muted'>Descripcion:".$row["descripcion"]."</small></p> ";
                    echo "
                    </div>  
                  </div>";
                    echo"</div>
                </div>  
                </div>";
                echo "
              </div>";
            }
  
                ?> 
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar Ventana</button>
      </div>
    </div>
  </div>
</div>


							<!-- Modal Recetario-->
<div class="modal fade fullscreen-modal fade" id="modalrecetas" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg"  role="document">
    <div class="modal-content" id="recetas">
      <div class="modal-header">
        <h4 class="modal-title text-center"  id="exampleModalLabel">Recetario</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
			<div id="accordion">			  
        <?php  
  require('./servicios/conection.php');
 $img=0;
 $sql = "SELECT * FROM recetas ORDER BY id_receta ASC";  
 $result = mysqli_query($link, $sql);   
     
                 while($row = mysqli_fetch_assoc($result)){
                  echo '<div class="card mb-3" >
                <div class="row no-gutters" style="background-color: rgb(245,245,245);">
                <div class="col-md-4">
                <img src="img/'.$img.'.jpg" width="300" height="300" background="#777" color="#777" class="img-thumbnail" style="margin:5%;">  
                </div>
                <div class="col-md-8" >
                  <div class="card-body" >
                  <div class="card-group" >
                    <div class="card-body text-justify" style="margin: 1px; padding: 1px;">';
                echo " <h5 class='card-title'>".$row["nombre"]."</h5>";
                    echo "<p><a><i class='icon-clock card-text'></i></a>".$row["tiempo"]."</p>";
                    echo "<p class='card-text'><small class='text-muted'>Dificultad:".$row["dificultad"]."</small></p> ";
                    echo " <p class='card-text'> Ingredientes: <small class='text-muted'>".$row["ingredientes"]."</small></p>
                    </div>  
                  </div>";
                    echo "<a class='btn  btn-outline-primary' style='float: right; margin:3%;'data-toggle='collapse'data-target='#collapse2' role='button' aria-expanded='false' aria-controls='collapse1'>Preparaci&oacute;n</a>";
                    echo"</div>
                </div>  
                </div>";
                echo "<div id='collapse2' class='collapse' aria-labelledby='headingTwo' data-parent='#accordion'>";
                echo "<hr class='featurette-divider'>";
                echo "<div class='card-body text-justify'>".$row["preparacion"]."";
                echo " </div>
            </div>
              </div>";
              $img++;
            }
  
                ?> 
				<!------------------------------CARRUSEL RECETAS--------------------------------------------------->
						 <div id="row2" class="row  justify-content-center">
				<div class="bd-example">
  <div id="CARRUSEL" class="carousel slide pointer-event" data-ride="carousel" data-interval="false">
    <ol class="carousel-indicators">
      <li data-target="#carouselExampleCaptions" data-slide-to="0" class=""></li>
      <li data-target="#carouselExampleCaptions" data-slide-to="1" class=""></li>
      <li data-target="#carouselExampleCaptions" data-slide-to="2" class="active"></li>
      <li data-target="#carouselExampleCaptions" data-slide-to="3" class=""></li>

    </ol>
    <div class="carousel-inner">
	
      <div class="carousel-item active">
		<iframe width="800" height="350" src="https://www.youtube.com/embed/WM69lk81bRU" frameborder="0" style="transform: translateX(-10%);"></iframe>        
      </div>
	  
      <div class="carousel-item">
        <iframe width="800" height="350" src="https://www.youtube.com/embed/FjqFDqcK6r8" frameborder="0" style="transform: translateX(-10%);"></iframe>
      </div>
	  
      <div class="carousel-item ">
		<iframe width="800" height="480" src="https://www.youtube.com/embed/QXThWqIiQ2k" frameborder="0" style="transform: translateX(-10%);"></iframe>       
      </div>
	  
      <div class="carousel-item">
	<iframe width="800" height="480" src="https://www.youtube.com/embed/DKupdxszg0E" frameborder="0" style="transform: translateX(-10%);"></iframe>    
      </div>
	  
    </div>
    <a class="carousel-control-prev" id="prev" href="#CARRUSEL" role="button" data-slide="prev">
      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" id="next" href="#CARRUSEL" role="button" data-slide="next">
      <span class="carousel-control-next-icon" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>
</div>
</div>
							
										</div>
		 
		 
		 
		
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar Ventana</button>
      </div>
    </div>
  </div>
</div>
<! -- ************************************ENDPOPUP************************************** -->
		

	<footer class="footer">
	
							  <div class="row no-gutters">
							  
								<div class="col-md-4">
									 <p></p>
									<div class="card-body">
									 <h5 id="contac"><i class="fa fa-road"></i>CONT&Aacute;CTANOS</h5>
										<div class="col-12">
											<p><a href="#"><i class="icon-phone text-white"></i></a>	Tel: 443-228-0290</p>
											<p><a href="#"><i class="icon-whatsapp text-white"></i></a>	Whatsapp: 443-228-0290</p>
											<p>Visita nuestras redes sociales:</p>
											<a href="https://www.facebook.com/Granja-de-Trucha-Tepetates-1125220357665723/" target="_blank"><i class="icon-facebook2 text-white"></i>	Granja "El Tepetate"</a>
										</div>
									</div> 
								</div>
								
								<div class="col-md-4"style=" background-color: #292c2f;">
											 <br>
										<h5><i class="fa fa-road"></i>ENCU&Eacute;NTRANOS EN:</h5>
										 <h5><i class="fa fa-road"></i>*Direcci&oacute;n*</h5>
										 <p></p>
										<div class="row">
											<div class="col-7">
												<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d15040.795312628848!2d-100.63540001501423!3d19.533075613119664!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x0!2zMTnCsDMxJzU5LjAiTiAxMDDCsDM3JzM2LjAiVw!5e0!3m2!1ses-419!2smx!4v1590545963170!5m2!1ses-419!2smx" width="300" height="280" frameborder="0" style="transform: translateY(-10px);border:0; border-radius: 15px;" allowfullscreen="true"></iframe>
											</div>
										</div> 
								</div>
								
								<div class="col-md-4" style=" background-color: #292c2f;">
											 <p></p>
											<div class="card-body">
																 <h5><i class="fa fa-road"></i>D&eacute;janos tu mensaje:</h5>
											<form action="./servicios/insert_comentario.php" method="POST">
												<fieldset class="form-group">
													<input type="email" class="form-control" id="exampleInputEmail1" name="email" placeholder="Introduce tu email">
												</fieldset>
												<fieldset class="form-group">
													<textarea class="form-control" id="exampleMessage" name="mensaje" placeholder="Mensaje" required></textarea>
												</fieldset>
												<fieldset class="form-group text-xs-right">
													<button type="submit" class="btn btn-secondary-outline btn-lg" name="Agregar">Enviar</button>
												</fieldset>
											</form>
											</div> 
								</div>
								
							  </div>					  
								

</footer>

		<script src="js/jquery.js"></script>
		<script src="js/bootstrap.min.js"></script>
  
</body>
</html>