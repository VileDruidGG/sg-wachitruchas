<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="stylesheet" href="css/bootstrap.min.css">
  <link href="css/simple-sidebar.css" rel="stylesheet">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
	<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
<script src="./charts/Chart.min.js"></script>
	<script src="./charts/utils.js"></script>

  <title>Monitoreo de alimento</title>

</head>

<body>

  <div class="d-flex" id="wrapper">
  
		<div class="bg-light border-right" id="sidebar-wrapper">
			  <div class="sidebar-heading">Granja El Tepetate </div>
			  <div class="list-group list-group-flush">
				<a href="./registro-gasto-ingreso.html" class="nav-link list-group-item-action bg-light">Registrar Gasto/Ingreso</a>
				<a href="./panel_administrador.php" 	class="nav-link list-group-item-action bg-light">Capital de Trabajo</a>
				<a href="./manejo-usuarios.php" 		class="nav-link list-group-item-action bg-light">Manejo de Usuarios</a>
				<a href="./estado-granja.php"		 	class="nav-link list-group-item-action bg-light">Estado de la Granja</a>
				<a href="./promociones.php"		 		class="nav-link list-group-item-action bg-light">Promociones</a>
				<a href="./recetario.php"		 		class="nav-link list-group-item-action bg-light">Recetario</a>
				<a href="./comentarios.php" 			class="nav-link list-group-item-action bg-light">Comentarios de Visitantes</a>
				<a href="./alimento.php"		 		class="nav-link bg-light">Monitoreo alimento</a> 
			  </div>
		</div>
	
		<div id="page-content-wrapper">
		  <nav class="navbar navbar-expand-lg navbar-light bg-dark border-bottom">
			<button class="btn btn-outline-secondary dropdown-toggle" id="menu-toggle">Menu</button>
			<ul class="navbar-nav ml-auto mt-2 mt-lg-0">
				<li class="nav-item text-nowrap">
					<a class="nav-link" style="color:white;" href="./index.php">Cerrar Sesión</a>
				</li>
			</ul>
		  </nav>
		  
		  <!----CONTENIDO---->
		  <main role="main">
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto|Varela+Round">
  <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
 
 
  </head>
  <body>
      <div class="container">
          <div class="table-wrapper">
              <div class="table-title">
                  <div class="row">
                      <div class="col-sm-6">
                        <h2>Monitoreo de Alimento</h2>
                      </div>
                      <div class="col-sm-6">
                        <a href="#addEmployeeModal" class="btn btn-success" data-toggle="modal"><i class="material-icons">&#xE147;</i> <span>Agregar Tipo de Alimento</span></a>
                        <a href="#editEmployeeModal" class="btn btn-success" data-toggle="modal"><i class="material-icons">&#xE147;</i> <span>Se compro alimento</span></a>
                         <a href="#deleteEmployeeModal" class="btn btn-danger" data-toggle="modal"><i class="material-icons">&#xE15C;</i> <span>Se uso alimento</span></a>
                       </div>
                       
                  </div>
              </div>
              <?php  
 require('./servicios/conection.php');
 $output = '';  
 $sql = "SELECT * FROM monitoreo ORDER BY id_alimento ASC";  
 $result = mysqli_query($link, $sql);   
     echo '<div class="table-responsive">  
           <table class="table table-striped table-hover">  
            <thead>
                <tr>
                          <th>ID </th>
                          <th>Nombre del Alimento </th>
                          <th>Cantidad(Kg) </th>
                          <th>Fecha de actualizacion</th>
                      </tr>
                </thead>';
                echo "<tbody>";
                 while($row = mysqli_fetch_assoc($result)){
                echo "<tr>";
                if($row["cantidad"] < 10){
                 echo" <script>alert('Un alimento esta por agotarse');</script>";
                }
                    echo "<td>".$row["id_alimento"]."</td>";
                    echo "<td>".$row["nombre"]."</td>";
                    echo "<td>".$row["cantidad"]."</td>";
                    echo "<td>".$row["fecha"]."</td>";
                echo "</tr>";
            }
        echo "</tbody>";
    echo "</table>";
    echo "</div>" ;
    ?>
  <!-- Add Modal HTML -->
    <div id="addEmployeeModal" class="modal fade">
      <div class="modal-dialog">
        <div class="modal-content">
          <form action="./servicios/insert_alimento.php" method="POST">           
            <div class="modal-header">            
              <h4 class="modal-title">Agregar tipo de alimento</h4>
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
            <div class="modal-body">  
            <div class="form-group">
                <label>Nombre del alimento</label>
                <input type="text" class="form-control" name="nombrea" required>
              </div>
              <div class="form-group">
                <label>Cantidad</label>
                <input type="text" class="form-control" name="cantidada" required>
              </div>     
            </div>
            <div class="modal-footer">
              <input type="button" class="btn btn-default" data-dismiss="modal" value="Cancelar">
              <input type="submit" class="btn btn-success" value="Agregar">
            </div>
          </form>
        </div>
      </div>
    </div>
    <!-- Edit Modal HTML -->
    <div id="editEmployeeModal" class="modal fade">
      <div class="modal-dialog">
        <div class="modal-content">
          <form action="./servicios/update_alimento.php" method="POST"> 
            <div class="modal-header">            
              <h4 class="modal-title">Se compro alimento</h4>
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
           <div class="modal-body">
              <div class="form-group">
                <label>ID del alimento</label>
                <input type="text" class="form-control" list="idsp" name="ida">
                  <datalist id="idsp">
                      <?php
                      require('./servicios/conection.php');
                      
                      $query="SELECT id_alimento FROM monitoreo";
                      $result=mysqli_query($link,$query);
                      while($row = mysqli_fetch_assoc($result)){
                          echo "<option value=".$row["id_alimento"].">";
                              }
                    ?>
                  </datalist>
              </div>      
              <div class="form-group">
                <label>Cantidad(Kg)</label>
                <input type="text" class="form-control" name="cantidada" required>
              </div>
              </div>
            <div class="modal-footer">
              <input type="button" class="btn btn-default" data-dismiss="modal" value="Cancelar">
              <input type="submit" class="btn btn-info" value="Guardar">
            </div>
          </form>
        </div>
      </div>
    </div>
    <!-- Delete Modal HTML -->
    <div id="deleteEmployeeModal" class="modal fade">
      <div class="modal-dialog">
        <div class="modal-content">
          <form action="./servicios/delete_alimento.php" method="POST">
            <div class="modal-header">            
              <h4 class="modal-title">Se uso alimento</h4>
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
           <div class="modal-body">
              <div class="form-group">
                <label>ID del alimento</label>
                <input type="text" class="form-control" list="idsp" name="ida">
                  <datalist id="idsp">
                      <?php
                    require('./servicios/conection.php');
                      $query="SELECT id_alimento FROM monitoreo";
                      $result=mysqli_query($link,$query);
                      while($row = mysqli_fetch_assoc($result)){
                          echo "<option value=".$row["id_alimento"].">";
                              }
                    ?>
                  </datalist>
              </div>      
              <div class="form-group">
                <label>Cantidad(Kg)</label>
                <input type="text" class="form-control" name="cantidada" required>
              </div>
              </div>
            <div class="modal-footer">
              <input type="button" class="btn btn-default" data-dismiss="modal" value="Cancelar">
              <input type="submit" class="btn btn-danger" value="Eliminar">
            </div>
          </form>
        </div>
      </div>
    </div>
  </body>
  </html>                                		                            
</main>

		</div>		
  </div>
  
  <script src="js/jquery.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script>
    $("#menu-toggle").click(function(e) {
      $("#wrapper").toggleClass("toggled");
    });
  </script>

</body>

</html>
